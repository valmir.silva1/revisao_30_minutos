#!/bin/bash

# Solicita ao usuário para inserir os números e a operação
read -p "Digite o primeiro número: " a
read -p "Digite a operação (+,-,*,/,**): " op
read -p "Digite o segundo número: " b


# Verifica a operação e calcula o resultado
if [ "$op" == "+" ]; then
    resultado=$((a + b))
elif [ "$op" == "-" ]; then
    resultado=$((a - b))
elif [ "$op" == "*" ]; then
    resultado=$((a * b))
elif [ "$op" == "/" ]; then
    resultado=$(awk "BEGIN {print $a / $b}")
elif [ "$op" == "**" ]; then
    resultado=$((a ** b))
else
    echo "Operação inválida!"
    exit 1
fi

# Imprime o resultado
echo "Resultado: $resultado"

