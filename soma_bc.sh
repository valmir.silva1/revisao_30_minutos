#!/bin/bash

# Solicita ao usuário para inserir os números
read -p "Digite o primeiro número: " num1
read -p "Digite o segundo número: " num2

# Usa o programa bc para calcular a soma
resultado=$(echo "$num1 + $num2" | bc)

# Imprime o resultado
echo "A soma de $num1 e $num2 é: $resultado"

