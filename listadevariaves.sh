#!/bin/bash

# Lista e explica 20 variáveis automáticas do BASH

# Variáveis do sistema
echo "1. \$0 - Nome do script: $0"
echo "2. \$1 - Primeiro argumento passado para o script: $1"
echo "3. \$2 - Segundo argumento passado para o script: $2"
echo "4. \$# - Número de argumentos passados: $#"
echo "5. \$@ - Lista de todos os argumentos passados como uma única string: $@"
echo "6. \$? - Código de retorno do último comando executado: $?"
echo "7. \$\$ - PID (Identificador do Processo) do script atual: $$"
echo "8. \$! - PID do último processo em segundo plano: $!"

# Variáveis do ambiente
echo "9. \$HOME - Diretório home do usuário atual: $HOME"
echo "10. \$USER - Nome do usuário atual: $USER"
echo "11. \$SHELL - Caminho para o shell atual: $SHELL"
echo "12. \$PWD - Diretório atual: $PWD"
echo "13. \$HOSTNAME - Nome do host: $HOSTNAME"

# Variáveis do terminal
echo "14. \$TERM - Tipo de terminal: $TERM"
echo "15. \$LINES - Número de linhas no terminal: $LINES"
echo "16. \$COLUMNS - Número de colunas no terminal: $COLUMNS"

# Variáveis do histórico de comandos
echo "17. \$HISTFILE - Caminho para o arquivo de histórico de comandos: $HISTFILE"
echo "18. \$HISTSIZE - Número máximo de comandos no histórico: $HISTSIZE"
echo "19. \$HISTCMD - Número do comando atual no histórico: $HISTCMD"

# Outras variáveis
echo "20. \$RANDOM - Número inteiro aleatório: $RANDOM"

