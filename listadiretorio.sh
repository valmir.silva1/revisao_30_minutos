#!/bin/bash

# Solicita ao usuário para inserir os nomes dos diretórios
read -p "Digite o nome do primeiro diretório: " dir1
read -p "Digite o nome do segundo diretório: " dir2

# Verifica se os diretórios existem
if [ ! -d "$dir1" ] || [ ! -d "$dir2" ]; then
    echo "Diretório não encontrado."
    exit 1
fi

# Lista os conteúdos dos diretórios e salva em o3.txt
ls "$dir1" > o3.txt
echo "-----" >> o3.txt
ls "$dir2" >> o3.txt

echo "Lista de arquivos dos diretórios foi salva em o3.txt."

